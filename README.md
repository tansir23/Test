# 一. 考勤管理制度
## （一）工作时间
  1.每周工作5天，每天工作8小时，上下班需打卡 ，上班打卡时间为08:30-9:30，若超过9:30算迟到，下班打卡时间为17:30-18:30，若早于17:30算早退，午休时间为12:30-13:30。  
  2.弹性上班时间，如因工作需要，周一至周四晚上加班超过21:30，则第二天上班时间可以推迟到10:30；超过24:00，则第二天上班时间可以推迟到11:30，该天为弹性上班时间。
## （二）公司假期规定
  公司假期类型包括事假，病假，年休假 ，婚假，丧假，补休假，产假(详情请参考公司考勤管理制度)。
## （三）请、销假制度及缺勤规定  
  1.请假流程：除病假或其他紧急情况的请假以外，至少提前一天通过企业微信提出申请。病假或其他紧急情况的请假，可通过企业微信提出申请或通过电话联系上司，获得同意后即可请假，通过企业微信提出申请后需发邮件至全员staff-ml@fenrir-inc.com.cn。  
  2.相关证明：病假一天以上需出示医院证明，其他假期应提供相应的有效证明。  
  3.加班调休申请：加班调休必须通过企业微信提前申请，未申请或审批未通过的调休，不承认调休，视为无薪事假处理。  
  4.缺勤规定：
>
迟到十分钟以内且每月三次以内不做处理。超过三次按照迟到十分钟以上一小时以内处理;  
迟到十分钟以上一小时以内按一小时计扣除基本工资和岗位津贴;  
迟到一小时以上按旷工半天计算;  
早退同迟到时间扣除方式相同;  
旷工半天扣除一天基本工资；旷工一天扣除两天基本工资；旷工两天扣除四天基本工资;  
连续旷工三天及累计一个月旷工次数达三次，作自动离职处理，不发放剩余工资。

# 二.公司保密制度
## （一）公司秘密范围及分级
### 1.公司秘密范围
（1）公司重大决策中的秘密事项。  
（2）公司尚未付诸实施的经营战略、方向、规划、项目及决策。  
（3）公司内部掌握的合同、协议、意向书及可行性报告、主要会议记录。  
（4）公司财务预决算报告及各类财务报表、统计报表。  
（5）公司掌握的尚未进入市场或尚未公开的各类信息。  
（6）公司职员的人事档案及人事信息。  
（7）其他公司确定保密的事项。
### 2.公司秘密等级
（1）绝密  
   指最重要的公司秘密，泄露会使公司的权益和利益遭受特别严重的损害。  
   公司经营发展中，直接影响公司权益和利益的重要决策文件资料为绝密级。  
（2）机密  
   指重要的公司秘密，泄露会使公司的权益和利益遭受严重的损害。  
   公司的规划、财务报表、统计资料、重要会议记录、公司经营情况为机密级。  
（3）秘密  
   指一般的公司秘密，泄露会使公司的权益和利益遭受损害。  
   公司人事档案、合同、协议、尚未进入市场或尚未公开的各类信息为秘密级。
## （二）保密措施
## （三）责任与处罚
## （四）应急保障
